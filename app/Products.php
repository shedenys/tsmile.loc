<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';
    protected $guarded = [];
    public $timestamps = false;

    public function productWarehouses() {
        return $this->hasMany('App\Warehouses', 'product_id');
    }
}
